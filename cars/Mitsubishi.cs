using System;

namespace My
{

    public class Mitsubishi : Cars
    {
        public Mitsubishi()
        {
            speed = 180;
            price = 7000;
            carName = "Mitsubishi Colt";
            transmission = "Автомат";
            p = "Врум-врум";
        }
    }
}