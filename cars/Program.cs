﻿using System;

namespace My
{

    public class Program
    {
        public static void Main()
        {  
            Jiguli vaz = new Jiguli();
            Mitsubishi colt = new Mitsubishi();


            List<Cars> spisok = new List<Cars>();
            spisok.Add(vaz);
            spisok.Add(colt);
            

            Console.WriteLine("В нашем автосалоне \"10-е колесо\" представлены следующие автомобили:\n");
            for(int i = 0; i < spisok.Count; i++)
            {
                Console.Write(i+1);
                var auto = spisok[i];
                auto.Description();
            }
            Console.WriteLine("");
                           
            Console.WriteLine("Пожалуйста выберите автомобиль \n");
            for(int i = 0; i < spisok.Count; i++)
            {
                var auto = spisok[i];
                Console.WriteLine($"Для выбора автомобиля {auto.carName} введите цифру \"{i+1}\".");
            }
               
            try
            {
                Cars auto;
                int key = Convert.ToInt32(Console.ReadLine());
                if (key <= 0 || key > spisok.Count)
                    {
                       Console.WriteLine("Неверный ввод!");
                    }
                else
                    {    
                        for(int i = 0; i < spisok.Count; i++)
                        {
                            if (key == i+1)
                            {
                                    auto = spisok[i];
                                    Console.WriteLine("Ну что, поехали?");
                                    Console.WriteLine("Нажмите Enter чтобы поехать.");
                                    Console.ReadLine();
                                    Cars.Poexali(auto.p);
                            }
                        
                        }
                    }
                
            }

            catch
            {
                Console.WriteLine("Ошибка. Введены некоректные данные!");
            }
            Console.WriteLine("Для завершения программы нажмите Enter.");
            Console.ReadLine();
            

        }
    }
}