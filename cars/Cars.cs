using System;

namespace My
{
    public class Cars
    {
        protected double _speed = 0;
        public double speed
        {
            get {return _speed;}
            set {_speed = value;}                    
        }
        protected double _price = 0;
        public double price
        {
          get {return _price;}
            set {_price = value;}
        }
        protected string _transmission = "";
        public string transmission
        {
            get {return _transmission;}
            set {_transmission = value;}
        }
        protected string _carName = "";
        public string carName
        {
            get {return _carName;}
            set {_carName = value;}
        }
        protected string _p = "Врум-врум...";
        public string p
        {
            get{return _p;}
            set{_p = value;}
        }

        
                        

        public void Description()
        {
            Console.WriteLine($"   Марка: {carName}; \n   Максимальная скорость: {speed}км/ч; \n   Коробка передач: {transmission}; \n   Цена: {price}$\n ");
        }
        
        public static void Poexali(string p) => Console.WriteLine(p);

    }
}