﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Jiguli : Cars
    {
        public Jiguli()
        {
            speed = 160;
            price = 3000;
            carName = "Жигули ВАЗ-2106";
            transmission = "Механика";
            p = "Тыр-тыр-тыр";
        }
    }
}
