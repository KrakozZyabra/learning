﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Jiguli vaz = new Jiguli();
            Mitsubishi colt = new Mitsubishi();
            
            List<Cars> spisok = new List<Cars>();
            spisok.Add(vaz);
            spisok.Add(colt);
                        

            Console.WriteLine("В нашем автосалоне \"10-е колесо\" представлены следующие автомобили:\n");
            for (int i = 0; i < spisok.Count; i++)
            {
                Console.WriteLine(i + 1);
                var auto = spisok[i];
                auto.Description();
            }
            Console.WriteLine("");

            Console.WriteLine("Пожалуйста выберите автомобиль: \n");
            for (int i = 0; i < spisok.Count; i++)
            {
                var auto = spisok[i];
                Console.WriteLine($"Для выбора автомобиля {auto.carName} введите цифру {i + 1}");
            }

            try
            {
                
                int key = Convert.ToInt32(Console.ReadLine());          
                if (key <= 0 || key > spisok.Count)                      
                {
                    Console.WriteLine("Неверный ввод!");
                }
                else                                                    // Убрал ненужный цикл, т.к. значение ключа мы знаем,    
                {                                                       // поэтому просто присваиваем "auto" экземпляр класса
                    Cars auto = spisok[key - 1];                             // из списка (по ключу)
                    Console.WriteLine("Ну что, поехали?");
                    Console.WriteLine("Нажмите Enter чтобы поехать.");
                    Console.ReadLine();
                    auto.Poexali(auto.p);                               // здесь тоже поправил, раньше тут использовался
                                                                        // статический метод, не спрашивай зачем =).
                }

            }

            catch
            {
                Console.WriteLine("Ошибка. Введены некоректные данные!");
            }
            

            Console.WriteLine("Для завершения программы нажмите Enter.");
            Console.ReadLine();
        }
    }
}
