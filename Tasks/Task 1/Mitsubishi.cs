﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Mitsubishi : Cars
    {
        public Mitsubishi()
        {
            speed = 180;
            price = 7000;
            carName = "Mitsubishi Colt";
            transmission = "Автомат";
            p = "Врум-врум";
        }
    }
}
